import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import './style.css'


const ProductComponent = () => {
    const products = useSelector((state) => state.allProducts.products);

    const renderList = products.map((product) => {
        const {id, title, image, price, category} = product;
        return (
            <div className="container__wrappers four wide column" key={id}>
                <div className="four wide column" >
                    <Link to={`/product/${id}`}>
                    <div className="ui link cards">
                        <div className="card">
                            <div className="image">
                                <img className="image__content" src={image} alt={title} />
                            </div>

                            <div className="content">
                                <div className="header">
                                  {products.map((product, index) =>
                                            index  < 1 && (
                                            <a className="text__title"  key={product.id}>{product.title}</a>
                                        )
                                     )}
                                </div>
                                <div className="meta price">$ {price}</div>
                                <div className="meta-description meta">{category}</div>
                            </div>
                        </div>
                    </div>   
                    </Link>           
                </div>
            </div>
        );
    })


    return <>
    {renderList}
    </>

};

export default ProductComponent
