import React from 'react'
import './style.css'

const Header = () => {
    return (
        <div className="ui fixed menu">
            <div className="ui container center">
                 <a href="/" className="logo">ShopTrends</a>
                <ul>
                    <a href="/" className="link">Home</a>
                    <a href="/portfolio" className="link">Portfolio</a>
                    <a href="/contact" className="link">Contact</a>
                    <a href="/blog" className="link">Blog</a>
                </ul>
            </div>
            <a href="/login" className="login_btn">Login</a>
        </div>
    )
}

export default Header
