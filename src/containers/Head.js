import React from 'react'
import './style.css'
import { Jumbotron, Button } from 'reactstrap'

const Head = () => {


    return (
        <div>
            <Jumbotron className="Jumbotron">
                <h1 className="display-3">Shop With Us!</h1>
                <p className="lead">We Bring to you Brands Of The Best Worlds.</p>
                <hr className="my-2" />
                <p className="bottom__text">Keep Checking For More To Come, While Continue Shopping In Our Store.</p>
                <p className="lead">
                <Button className="btn__primary" color="primary">Learn More</Button>
                </p>
            </Jumbotron>
        </div>
    )
}

export default Head
