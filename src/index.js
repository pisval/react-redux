import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//Importing the Provider
import { Provider } from 'react-redux'
import App from './App';

//Importing the created store class
import store from './redux/store'


ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
         <App />
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
